cube(`Orders`, {
  sql: `
      select 1 as id, 100 as amount, 'new' status, DATE('2020-01-01') as timestamp
        UNION ALL
      select 2 as id, 200 as amount, 'new' status, DATE('2020-01-01') as timestamp
        UNION ALL
      select 3 as id, 300 as amount, 'processed' status, DATE('2020-01-01') as timestamp
        UNION ALL
      select 4 as id, 500 as amount, 'processed' status, DATE('2020-01-01') as timestamp
        UNION ALL
      select 5 as id, 600 as amount, 'shipped' status, DATE('2020-01-01') + interval '48 hour' as timestamp
    `,

  measures: {
    count1: {
      type: `count`,
    },

    count2: {
      type: `count`,
    },

    count3: {
      type: `count`,
    },

    totalAmount: {
      sql: `amount`,
      type: `sum`,
    },
  },

  dimensions: {
    status1: {
      sql: `status`,
      type: `string`,
    },

    status2: {
      sql: `status`,
      type: `string`,
    },

    status3: {
      sql: `status`,
      type: `string`,
    },

    timestamp: {
      sql: `timestamp`,
      type: `time`,
    },
  },

  preAggregations: {
    noParts: {
      type: `rollup`,
      measureReferences: [count1, totalAmount],
      dimensionReferences: [status1],
      scheduledRefresh: false,
      external: true,
      refreshKey: {
        every: `1 hours`,
      },
      timeDimensionReference: timestamp,
      granularity: `hour`,
    },
    partsNoRange: {
      type: `rollup`,
      measureReferences: [count2, totalAmount],
      dimensionReferences: [status2],
      scheduledRefresh: false,
      external: true,
      refreshKey: {
        every: `1 hours`,
      },
      timeDimensionReference: timestamp,
      granularity: `hour`,
      partitionGranularity: `hour`,
    },
    partsRange: {
      type: `rollup`,
      measureReferences: [count3, totalAmount],
      dimensionReferences: [status3],
      scheduledRefresh: false,
      external: true,
      refreshKey: {
        every: `1 hours`,
      },
      buildRangeStart: {
        sql: `SELECT DATE('2020-01-01')`,
      },
      buildRangeEnd: {
        sql: `SELECT DATE('2020-01-15')`,
      },
      timeDimensionReference: timestamp,
      granularity: `hour`,
      partitionGranularity: `day`,
    },
  },
});
//
